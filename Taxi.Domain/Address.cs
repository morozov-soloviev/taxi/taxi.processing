﻿using System;

namespace Taxi.Domain
{
    public class Address
    {
        public Guid Id { get; set; }
        public Street Street { get; set; }
        public string BuildingNumber { get; set; }
        public string SpecificAddress { get; set; }
        public Position Position { get; set; }
    }
}
