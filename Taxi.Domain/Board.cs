﻿using System;

namespace Taxi.Domain
{
    public class Board
    {
        public Guid Id { get; set; }
        public Driver Driver { get; set; }
        public Car Car { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public Position Position { get; set; }
    }
}
