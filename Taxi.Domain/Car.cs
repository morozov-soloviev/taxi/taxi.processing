﻿using System;
using System.Collections.Generic;

namespace Taxi.Domain
{
    public class CarCarOption
    {
        public CarOption CarOption { get; set; }
    }

    public class Car
    {
        public Guid Id { get; set; }
        public User Owner { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public CarType Type { get; set; }
        public ICollection<CarCarOption> Options { get; set; }
        public string Number { get; set; }
        public DateTime Year { get; set; }
        public int TarifRank { get; set; }
    }
}
