﻿using System;

namespace Taxi.Domain
{
    public class CarOption
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
