﻿using System;

namespace Taxi.Domain
{
    public class CarType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
