﻿using System;

namespace Taxi.Domain
{
    public class City
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Region Region { get; set; }
    }
}
