﻿using System;
using System.Collections.Generic;

namespace Taxi.Domain
{
    public class Client
    {
        public Guid Id { get; set; }
        public User User { get; set; }
        public ICollection<ClientAddress> Addresses { get; set; }
    }
}
