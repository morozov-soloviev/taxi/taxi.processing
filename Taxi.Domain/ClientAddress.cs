﻿using System;

namespace Taxi.Domain
{
    public class ClientAddress
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Address Address { get; set; }
    }
}
