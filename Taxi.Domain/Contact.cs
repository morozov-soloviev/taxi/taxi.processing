﻿using System;

namespace Taxi.Domain
{
    public class Contact
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender? Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
