﻿using System;

namespace Taxi.Domain
{
    public class Country
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
