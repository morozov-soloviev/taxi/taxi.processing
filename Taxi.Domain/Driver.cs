﻿using System;

namespace Taxi.Domain
{
    public class Driver
    {
        public Guid Id { get; set; }
        public User User { get; set; }
        public string LicenceNumber { get; set; }
        public bool IsActive { get; set; }
    }
}
