﻿using System;
using System.Collections.Generic;

namespace Taxi.Domain
{
    public class OrderCarType
    {
        public CarType CarType { get; set; }
    }

    public class OrderCarOption
    {
        public CarOption CarOption { get; set; }
    }
    public class Order
    {
        public Guid Id { get; set; }
        public Board Board { get; set; }
        public Client Client { get; set; }
        public IList<RouteMember> Route { get; set; }
        public Tarif Tarif { get; set; }
        public ICollection<OrderCarType> CarTypes { get; set; }
        public ICollection<OrderCarOption> CarOptions { get; set; }
        public double CalculatedAmount { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public OrderStatus Status { get; set; }
    }
}
