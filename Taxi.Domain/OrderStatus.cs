﻿namespace Taxi.Domain
{
    public enum OrderStatus
    {
        Created = 0,
        OnWayToClient = 1,
        Waiting = 2,
        OnWay = 3,
        Completed = 4,
        Canceled = 5
    }
}
