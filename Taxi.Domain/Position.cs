﻿using System;

namespace Taxi.Domain
{
    public class Position
    {
        public Guid Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
