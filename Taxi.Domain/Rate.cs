﻿using System;

namespace Taxi.Domain
{
    public class Rate
    {
        public Guid Id { get; set; }
        public Order Order { get; set; }
        public int Score { get; set; }
        public string Comment { get; set; }
    }
}
