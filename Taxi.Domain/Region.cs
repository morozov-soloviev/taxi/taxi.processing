﻿using System;

namespace Taxi.Domain
{
    public class Region
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }
    }
}
