﻿using System;

namespace Taxi.Domain
{
    public class RouteMember
    {
        public Guid Id { get; set; }
        public Address Address { get; set; }
        public int OrderIndex { get; set; }
        public DateTime? ArriveDate { get; set; }
    }
}
