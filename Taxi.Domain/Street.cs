﻿using System;

namespace Taxi.Domain
{
    public class Street
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public City City { get; set; }
    }
}
