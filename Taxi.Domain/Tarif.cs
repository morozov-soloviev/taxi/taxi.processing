﻿using System;

namespace Taxi.Domain
{
    public class Tarif
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Region Region { get; set; }
        public int Rank { get; set; }
        public double DistancePrice { get; set; }
        public double TimePrice { get; set; }
    }
}
