﻿using System;

namespace Taxi.Domain
{
    public class User
    {
        
        public Guid Id { get; set; }
        public Contact Contact { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsActive { get; set; }
    }
}
