﻿namespace Taxi.WebApi.Authorization
{
    public class AuthConstants
    {
        public const string ClientTokenRole = "client";
        public const string DriverTokenRole = "driver";
        public const string UserTokenRole = "user";
    }
}
