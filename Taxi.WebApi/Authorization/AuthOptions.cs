﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Taxi.WebApi.Authorization
{
    public class AuthOptions
    {
        public IConfiguration Configuration { get; }

        public AuthOptions(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string ISSUER
        {
            get { return Configuration.GetValue<string>("AUTH_ISSUER"); }
        }
        public string AUDIENCE
        {
            get { return Configuration.GetValue<string>("AUTH_AUDIENCE"); }
        }
        private string KEY
        {
            get { return Configuration.GetValue<string>("AUTH_KEY"); }
        }
        public int LIFETIME
        {
            get { return Configuration.GetValue<int>("AUTH_LIFETIME"); }
        }
        public SymmetricSecurityKey SymmetricSecurityKey
        {
            get { return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY)); }
        }
    }
}
