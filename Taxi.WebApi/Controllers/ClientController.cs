﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Samarasoft.Common.WebApi;
using Taxi.WebApi.Authorization;

namespace Taxi.WebApi.Controllers
{
    [ApiController]
    [Route("client/")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class ClientController : ControllerBase
    {
        private readonly Model.Client.Info.Handler _infoHandler;
        private readonly Model.Client.Order.Calculate.Handler _calculateHandler;
        private readonly Model.Client.Order.Create.Handler _createHandler;
        private readonly Model.Client.Order.Cancel.Handler _cancelHandler;
        private readonly Model.Client.Order.Info.Handler _orderInfoHandler;
        private readonly Model.Client.Order.List.Handler _listHandler;
        private readonly Model.Client.User.Register.Handler _registerHandler;
        private readonly Model.Client.User.Login.Handler _loginHandler;
        private readonly Model.Client.User.Update.Handler _updateHandler;
        public ClientController(Model.Client.Info.Handler infoHandler,
            Model.Client.Order.Calculate.Handler calculateHandler,
            Model.Client.Order.Create.Handler createHandler,
            Model.Client.Order.Cancel.Handler cancelHandler,
            Model.Client.Order.Info.Handler orderInfoHandler,
            Model.Client.Order.List.Handler listHandler,
            Model.Client.User.Register.Handler registerHandler,
            Model.Client.User.Login.Handler loginHandler,
            Model.Client.User.Update.Handler updateHandler)
        {
            _infoHandler = infoHandler;
            _calculateHandler = calculateHandler;
            _createHandler = createHandler;
            _cancelHandler = cancelHandler;
            _orderInfoHandler = orderInfoHandler;
            _listHandler = listHandler;
            _registerHandler = registerHandler;
            _loginHandler = loginHandler;
            _updateHandler = updateHandler;
        }

        [ProducesResponseType(typeof(ApiResult<Model.Client.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение основной информации клиента.")]
        [Authorize(Roles = AuthConstants.ClientTokenRole)]
        [HttpGet("info")]
        public ApiResult<Model.Client.Info.ResultDto> Info()
        {
            var result = _infoHandler.Handle(User.Identity.Name);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Client.Info.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Client.Order.Calculate.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Расчет стоимости заказа.")]
        [Authorize(Roles = AuthConstants.ClientTokenRole)]
        [HttpPost("order/calculate")]//должен быть get
        public ApiResult<Model.Client.Order.Calculate.ResultDto> Calculate([FromBody] Model.Client.Order.Calculate.Query query)
        {
            var result = _calculateHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Client.Order.Calculate.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Client.Order.Create.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Создание заказа клиентом.")]
        [Authorize(Roles = AuthConstants.ClientTokenRole)]
        [HttpPost("order/create")]
        public ApiResult<Model.Client.Order.Create.ResultDto> Create([FromBody] Model.Client.Order.Create.Query query)
        {
            var result = _createHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Client.Order.Create.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Отмена заказа клиентом.")]
        [Authorize(Roles = AuthConstants.ClientTokenRole)]
        [HttpPost("order/cancel")]
        public ApiResult Cancel([FromBody] Model.Client.Order.Cancel.Query query)
        {
            var result = _cancelHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Client.Order.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации о заказе клиента.")]
        [Authorize(Roles = AuthConstants.ClientTokenRole)]
        [HttpPost("order/info")]//должен быть get
        public ApiResult<Model.Client.Order.Info.ResultDto> OrderInfo([FromBody] Model.Client.Order.Info.Query query)
        {
            var result = _orderInfoHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Client.Order.Info.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Client.Order.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка заказов клиента.")]
        [Authorize(Roles = AuthConstants.ClientTokenRole)]
        [HttpGet("order/list")]
        public ApiResult<Model.Client.Order.List.ResultDto> List()
        {
            var result = _listHandler.Handle(User.Identity.Name);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Client.Order.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [Description("Обновление данных клиента.")]
        [Authorize(Roles = AuthConstants.ClientTokenRole)]
        [HttpPost("user/update")]
        public ApiResult Update([FromBody] Model.Client.User.Update.Query query)
        {
            var result = _updateHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [Description("Регистрация клиента.")]
        [HttpPost("user/register")]
        public ApiResult Register([FromBody] Model.Client.User.Register.Query query)
        {
            var result = _registerHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Client.User.Login.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [Description("Вход клиента.")]
        [HttpPost("user/login")]
        public ApiResult<Model.Client.User.Login.ResultDto> Login([FromBody] Model.Client.User.Login.Query query)
        {
            var result = _loginHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Client.User.Login.ResultDto>(result.ErrorCode);
        }
    }
}
