﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Samarasoft.Common.WebApi;
using Taxi.WebApi.Authorization;

namespace Taxi.WebApi.Controllers
{
    [ApiController]
    [Route("data/")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class DataController : ControllerBase
    {
        private readonly Model.Data.User.List.Handler _userListHandler;
        private readonly Model.Data.User.Save.Handler _userSaveHandler;
        private readonly Model.Data.User.Info.Handler _userInfoHandler;
        private readonly Model.Data.Driver.List.Handler _driverListHandler;
        private readonly Model.Data.Driver.Save.Handler _driverSaveHandler;
        private readonly Model.Data.Driver.Info.Handler _driverInfoHandler;
        private readonly Model.Data.Tarif.List.Handler _tarifListHandler;
        private readonly Model.Data.Tarif.Save.Handler _tarifSaveHandler;
        private readonly Model.Data.Tarif.Info.Handler _tarifInfoHandler;
        private readonly Model.Data.Order.List.Handler _orderListHandler;
        private readonly Model.Data.Order.Info.Handler _orderInfoHandler;
        private readonly Model.Data.Car.List.Handler _carListHandler;
        private readonly Model.Data.Car.Save.Handler _carSaveHandler;
        private readonly Model.Data.Car.Info.Handler _carInfoHandler;
        private readonly Model.Data.CarOption.List.Handler _carOptionListHandler;
        private readonly Model.Data.CarOption.Save.Handler _carOptionSaveHandler;
        private readonly Model.Data.CarOption.Info.Handler _carOptionInfoHandler;
        private readonly Model.Data.CarType.List.Handler _carTypeListHandler;
        private readonly Model.Data.CarType.Save.Handler _carTypeSaveHandler;
        private readonly Model.Data.CarType.Info.Handler _carTypeInfoHandler;
        public DataController(Model.Data.User.List.Handler userListHandler,
            Model.Data.User.Save.Handler userSaveHandler,
            Model.Data.User.Info.Handler userInfoHandler,
            Model.Data.Driver.List.Handler driverListHandler,
            Model.Data.Driver.Save.Handler driverSaveHandler,
            Model.Data.Driver.Info.Handler driverInfoHandler,
            Model.Data.Tarif.List.Handler tarifListHandler,
            Model.Data.Tarif.Save.Handler tarifSaveHandler,
            Model.Data.Tarif.Info.Handler tarifInfoHandler,
            Model.Data.Order.List.Handler orderListHandler,
            Model.Data.Order.Info.Handler orderInfoHandler,
            Model.Data.Car.List.Handler carListHandler,
            Model.Data.Car.Save.Handler carSaveHandler,
            Model.Data.Car.Info.Handler carInfoHandler,
            Model.Data.CarOption.List.Handler carOptionListHandler,
            Model.Data.CarOption.Save.Handler carOptionSaveHandler,
            Model.Data.CarOption.Info.Handler carOptionInfoHandler,
            Model.Data.CarType.List.Handler carTypeListHandler,
            Model.Data.CarType.Save.Handler carTypeSaveHandler,
            Model.Data.CarType.Info.Handler carTypeInfoHandler)
        {
            _userListHandler = userListHandler;
            _userSaveHandler = userSaveHandler;
            _userInfoHandler = userInfoHandler;
            _driverListHandler = driverListHandler;
            _driverSaveHandler = driverSaveHandler;
            _driverInfoHandler = driverInfoHandler;
            _tarifListHandler = tarifListHandler;
            _tarifSaveHandler = tarifSaveHandler;
            _tarifInfoHandler = tarifInfoHandler;
            _orderListHandler = orderListHandler;
            _orderInfoHandler = orderInfoHandler;
            _carListHandler = carListHandler;
            _carSaveHandler = carSaveHandler;
            _carInfoHandler = carInfoHandler;
            _carOptionListHandler = carOptionListHandler;
            _carOptionSaveHandler = carOptionSaveHandler;
            _carOptionInfoHandler = carOptionInfoHandler;
            _carTypeListHandler = carTypeListHandler;
            _carTypeSaveHandler = carTypeSaveHandler;
            _carTypeInfoHandler = carTypeInfoHandler;
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.User.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка пользователей.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpGet("user/list")]
        public ApiResult<Model.Data.User.List.ResultDto> UserList()
        {
            var result = _userListHandler.Handle();
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.User.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Изменение пользователя.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("user/save")]
        public ApiResult UserSave([FromBody] Model.Data.User.Save.Query query)
        {
            var result = _userSaveHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.User.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации о пользователе.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("user/info")]//должен быть get
        public ApiResult<Model.Data.User.Info.ResultDto> UserInfo([FromBody] Model.Data.User.Info.Query query)
        {
            var result = _userInfoHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.User.Info.ResultDto>(result.ErrorCode);
        }


        [ProducesResponseType(typeof(ApiResult<Model.Data.Driver.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка водителей.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpGet("driver/list")]
        public ApiResult<Model.Data.Driver.List.ResultDto> DriverList()
        {
            var result = _driverListHandler.Handle();
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Driver.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Изменение водителя.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("driver/save")]
        public ApiResult DriverSave([FromBody] Model.Data.Driver.Save.Query query)
        {
            var result = _driverSaveHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.Driver.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации о водителе.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("driver/info")]//должен быть get
        public ApiResult<Model.Data.Driver.Info.ResultDto> DriverInfo([FromBody] Model.Data.Driver.Info.Query query)
        {
            var result = _driverInfoHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Driver.Info.ResultDto>(result.ErrorCode);
        }


        [ProducesResponseType(typeof(ApiResult<Model.Data.Tarif.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка тарифов.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpGet("tarif/list")]
        public ApiResult<Model.Data.Tarif.List.ResultDto> TarifList()
        {
            var result = _tarifListHandler.Handle();
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Tarif.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Изменение тарифа.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("tarif/save")]
        public ApiResult TarifSave([FromBody] Model.Data.Tarif.Save.Query query)
        {
            var result = _tarifSaveHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.Tarif.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации о тарифе.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("tarif/info")]//должен быть get
        public ApiResult<Model.Data.Tarif.Info.ResultDto> TarifInfo([FromBody] Model.Data.Tarif.Info.Query query)
        {
            var result = _tarifInfoHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Tarif.Info.ResultDto>(result.ErrorCode);
        }


        [ProducesResponseType(typeof(ApiResult<Model.Data.Order.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка заказов.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpGet("order/list")]
        public ApiResult<Model.Data.Order.List.ResultDto> OrderList()
        {
            var result = _orderListHandler.Handle();
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Order.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.Order.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации о заказе.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("order/info")]//должен быть get
        public ApiResult<Model.Data.Order.Info.ResultDto> OrderInfo([FromBody] Model.Data.Order.Info.Query query)
        {
            var result = _orderInfoHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Order.Info.ResultDto>(result.ErrorCode);
        }


        [ProducesResponseType(typeof(ApiResult<Model.Data.Car.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpGet("car/list")]
        public ApiResult<Model.Data.Car.List.ResultDto> CarList()
        {
            var result = _carListHandler.Handle();
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Car.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Изменение авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("car/save")]
        public ApiResult CarSave([FromBody] Model.Data.Car.Save.Query query)
        {
            var result = _carSaveHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.Car.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации об авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("car/info")]//должен быть get
        public ApiResult<Model.Data.Car.Info.ResultDto> CarInfo([FromBody] Model.Data.Car.Info.Query query)
        {
            var result = _carInfoHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.Car.Info.ResultDto>(result.ErrorCode);
        }


        [ProducesResponseType(typeof(ApiResult<Model.Data.CarOption.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка опций авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpGet("carOption/list")]
        public ApiResult<Model.Data.CarOption.List.ResultDto> CarOptionList()
        {
            var result = _carOptionListHandler.Handle();
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.CarOption.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Изменение опции авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("carOption/save")]
        public ApiResult CarOptionSave([FromBody] Model.Data.CarOption.Save.Query query)
        {
            var result = _carOptionSaveHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.CarOption.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации об опции авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("carOption/info")]//должен быть get
        public ApiResult<Model.Data.CarOption.Info.ResultDto> CarOptionInfo([FromBody] Model.Data.CarOption.Info.Query query)
        {
            var result = _carOptionInfoHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.CarOption.Info.ResultDto>(result.ErrorCode);
        }


        [ProducesResponseType(typeof(ApiResult<Model.Data.CarType.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка типов авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpGet("carType/list")]
        public ApiResult<Model.Data.CarType.List.ResultDto> CarTypeList()
        {
            var result = _carTypeListHandler.Handle();
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.CarType.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Изменение типа авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("carType/save")]
        public ApiResult CarTypeSave([FromBody] Model.Data.CarType.Save.Query query)
        {
            var result = _carTypeSaveHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Data.CarType.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации о типе авто.")]
        [Authorize(Roles = AuthConstants.UserTokenRole)]
        [HttpPost("carType/info")]//должен быть get
        public ApiResult<Model.Data.CarType.Info.ResultDto> CarTypeInfo([FromBody] Model.Data.CarType.Info.Query query)
        {
            var result = _carTypeInfoHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Data.CarType.Info.ResultDto>(result.ErrorCode);
        }
    }
}
