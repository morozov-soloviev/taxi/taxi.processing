﻿using System.ComponentModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Samarasoft.Common.WebApi;
using Taxi.WebApi.Authorization;

namespace Taxi.WebApi.Controllers
{
    [ApiController]
    [Route("driver/")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class DriverController : ControllerBase
    {
        private readonly Model.Driver.Info.Handler _infoHandler;
        private readonly Model.Driver.Order.Take.Handler _createHandler;
        private readonly Model.Driver.Order.Complete.Handler _completeHandler;
        private readonly Model.Driver.Order.Cancel.Handler _cancelHandler;
        private readonly Model.Driver.Order.Update.Handler _orderUpdateHandler;
        private readonly Model.Driver.Order.Info.Handler _orderInfoHandler;
        private readonly Model.Driver.Order.List.Handler _listHandler;
        private readonly Model.Driver.Order.Available.Handler _availableHandler;
        private readonly Model.Driver.User.Update.Handler _userUpdateHandler;
        private readonly Model.Driver.User.Register.Handler _registerHandler;
        private readonly Model.Driver.User.Login.Handler _loginHandler;
        private readonly Model.Driver.Board.Info.Handler _boardInfoHandler;
        private readonly Model.Driver.Board.Start.Handler _startHandler;
        private readonly Model.Driver.Board.End.Handler _endHandler;
        private readonly Model.Driver.Board.Update.Handler _boardUpdateHandler;
        public DriverController(Model.Driver.Info.Handler infoHandler,
            Model.Driver.Order.Take.Handler createHandler,
            Model.Driver.Order.Complete.Handler completeHandler,
            Model.Driver.Order.Cancel.Handler cancelHandler,
            Model.Driver.Order.Update.Handler orderUpdateHandler,
            Model.Driver.Order.Info.Handler orderInfoHandler,
            Model.Driver.Order.List.Handler listHandler,
            Model.Driver.Order.Available.Handler availableHandler,
            Model.Driver.User.Update.Handler userUpdateHandler,
            Model.Driver.User.Register.Handler registerHandler,
            Model.Driver.User.Login.Handler loginHandler,
            Model.Driver.Board.Info.Handler boardInfoHandler,
            Model.Driver.Board.Start.Handler startHandler,
            Model.Driver.Board.End.Handler endHandler,
            Model.Driver.Board.Update.Handler boardUpdateHandle)
        {
            _infoHandler = infoHandler;
            _createHandler = createHandler;
            _completeHandler = completeHandler;
            _cancelHandler = cancelHandler;
            _orderUpdateHandler = orderUpdateHandler;
            _orderInfoHandler = orderInfoHandler;
            _listHandler = listHandler;
            _availableHandler = availableHandler;
            _userUpdateHandler = userUpdateHandler;
            _registerHandler = registerHandler;
            _loginHandler = loginHandler;
            _boardInfoHandler = boardInfoHandler;
            _startHandler = startHandler;
            _endHandler = endHandler;
            _boardUpdateHandler = boardUpdateHandle;
        }

        [ProducesResponseType(typeof(ApiResult<Model.Driver.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение основной информации водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpGet("info")]
        public ApiResult<Model.Driver.Info.ResultDto> Info()
        {
            var result = _infoHandler.Handle(User.Identity.Name);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Driver.Info.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Выбор заказа водителем.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("order/take")]
        public ApiResult Take([FromBody] Model.Driver.Order.Take.Query query)
        {
            var result = _createHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Завершение заказа водителем.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("order/complete")]
        public ApiResult Complete([FromBody] Model.Driver.Order.Complete.Query query)
        {
            var result = _completeHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Отмена заказа водителем.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("order/cancel")]
        public ApiResult Cancel([FromBody] Model.Driver.Order.Cancel.Query query)
        {
            var result = _cancelHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Обновление информции рейса водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("order/update")]
        public ApiResult Update([FromBody] Model.Driver.Order.Update.Query query)
        {
            var result = _orderUpdateHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Driver.Order.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение нформации о заказе водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("order/info")]//должен быть get
        public ApiResult<Model.Driver.Order.Info.ResultDto> OrderInfo([FromBody] Model.Driver.Order.Info.Query query)
        {
            var result = _orderInfoHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Driver.Order.Info.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Driver.Order.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка заказов водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpGet("order/list")]
        public ApiResult<Model.Driver.Order.List.ResultDto> List()
        {
            var result = _listHandler.Handle(User.Identity.Name);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Driver.Order.List.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Driver.Order.List.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Получение списка заказов водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpGet("order/available")]
        public ApiResult<Model.Driver.Order.Available.ResultDto> Available()
        {
            var result = _availableHandler.Handle(User.Identity.Name);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Driver.Order.Available.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Driver.Board.Info.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Информация о рейсе водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpGet("board/info")]
        public ApiResult<Model.Driver.Board.Info.ResultDto> BoardInfo()
        {
            var result = _boardInfoHandler.Handle(User.Identity.Name);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult<Model.Driver.Board.Info.ResultDto>.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Начало рейса водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("board/start")]
        public ApiResult Start([FromBody] Model.Driver.Board.Start.Query query)
        {
            var result = _startHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Завершение рейса водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("board/end")]
        public ApiResult End()
        {
            var result = _endHandler.Handle(User.Identity.Name);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Обновление информции рейса водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("board/update")]
        public ApiResult Update([FromBody] Model.Driver.Board.Update.Query query)
        {
            var result = _boardUpdateHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [Description("Регистрация водителя.")]
        [HttpPost("user/register")]
        public ApiResult Register([FromBody] Model.Driver.User.Register.Query query)
        {
            var result = _registerHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult<Model.Driver.User.Login.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [Description("Вход водителя.")]
        [HttpPost("user/login")]
        public ApiResult<Model.Driver.User.Login.ResultDto> Login([FromBody] Model.Driver.User.Login.Query query)
        {
            var result = _loginHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.Driver.User.Login.ResultDto>(result.ErrorCode);
        }

        [ProducesResponseType(typeof(ApiResult), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [ProducesResponseType(typeof(ApiResult), 403)]
        [Description("Обновление данных водителя.")]
        [Authorize(Roles = AuthConstants.DriverTokenRole)]
        [HttpPost("user/update")]
        public ApiResult Update([FromBody] Model.Driver.User.Update.Query query)
        {
            var result = _userUpdateHandler.Handle(User.Identity.Name, query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Warnings);
            }
            return ApiResult.Fail(result.ErrorCode);
        }
    }
}
