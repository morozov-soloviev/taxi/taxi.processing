﻿using System.ComponentModel;
using Microsoft.AspNetCore.Mvc;
using Samarasoft.Common.WebApi;

namespace Taxi.WebApi.Controllers
{
    [ApiController]
    [Route("user/")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class UserController : ControllerBase
    {
        private readonly Model.User.Login.Handler _loginHandler;
        public UserController(
            Model.User.Login.Handler loginHandler)
        {
            _loginHandler = loginHandler;
        }

        [ProducesResponseType(typeof(ApiResult<Model.User.Login.ResultDto>), 200)]
        [ProducesResponseType(typeof(ApiResult), 400)]
        [ProducesResponseType(typeof(ApiResult), 401)]
        [Description("Вход пользователя.")]
        [HttpPost("login")]
        public ApiResult<Model.User.Login.ResultDto> Login([FromBody] Model.User.Login.Query query)
        {
            var result = _loginHandler.Handle(query);
            if (result.IsSuccess)
            {
                return ApiResult.Ok(result.Value, result.Warnings);
            }
            return ApiResult.Fail<Model.User.Login.ResultDto>(result.ErrorCode);
        }
    }
}
