﻿namespace Taxi.WebApi
{
    public static class Errors
    {
        public const string Default = "internal_error_:(";
        public const string UserAlreadyExist = "user_already_exist";
        public const string UserNotFound = "user_not_found";
        public const string WrongLoginOrPassword = "wrong_login_or_password";
        public const string UserNeedActivation = "user_need_activation";
        public const string UserAccessDenied = "user_access_denied";
        public const string IncorrectId = "incorrect_identifier";
        public const string NoActiveBoard = "no_active_board";
        public const string BoardAlreadyActive = "board_already_active";
        public const string AlreadyHaveActiveOrder = "already_have_active_order";
        public const string InvalidOrderStatus = "invalid_order_status";
    }
}
