﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Taxi.WebApi.Migrations
{
    public partial class RouteOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderIndex",
                table: "RouteMember",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("555537dd-aa9e-49ae-95c7-e4cbb2307fbd"),
                column: "Password",
                value: "$2a$11$f3WtFCSAdStJhj9thqGZG.BZGVTSpQI7.qQFGstsdXt/tvclORcti");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderIndex",
                table: "RouteMember");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("555537dd-aa9e-49ae-95c7-e4cbb2307fbd"),
                column: "Password",
                value: "Supervisor");
        }
    }
}
