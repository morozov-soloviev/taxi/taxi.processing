﻿using System.Collections.Generic;
using System.ComponentModel;
using Taxi.Domain;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Client.Info
{
    public class ResultDto
    {
        [Description("Клиент")]
        public Contact Client { get; set; }

        [Description("Список тарифов")]
        public IList<TarifDto> Tarifs { get; set; }

        [Description("Список типов авто")]
        public IList<CarType> CarTypes { get; set; }

        [Description("Список опций")]
        public IList<CarOption> CarOptions { get; set; }

        [Description("Текущий заказ")]
        public OrderDto CurrentOrder { get; set; }
    }
}
