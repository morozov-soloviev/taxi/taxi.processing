﻿using System;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Client.Order.Calculate
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly CalculateOrderService _calculateOrderService;
        private readonly AddressConverter _addressConverter;
        public Handler(TaxiContext context, CalculateOrderService calculateOrderService, AddressConverter addressConverter)
        {
            _storage = context;
            _calculateOrderService = calculateOrderService;
            _addressConverter = addressConverter;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var routeResult = _addressConverter.CovertToAddress(query.Route);
            if (!routeResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(routeResult.ErrorCode);
            }

            var route = routeResult.Value.Select(a => new RouteMember
            {
                Id = Guid.NewGuid(),
                Address = a,
                ArriveDate = null
            }).ToList();

            var tarif = _storage.Tarif.FirstOrDefault(t => t.Id == query.TarifId);
            if (tarif == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            var calculateResult = _calculateOrderService.Calculate(tarif, route.Select(m => m.Address).ToList());
            if (!calculateResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(calculateResult.ErrorCode);
            }

            return Result<ResultDto>.Success(new ResultDto
            {
                Amount = calculateResult.Value
            });
        }
    }
}
