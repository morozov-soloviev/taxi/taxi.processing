﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Client.Order.Calculate
{
    public class Query
    {
        [Required]
        [Description("Маршрут")]
        public AddressDto[] Route { get; set; }

        [Required]
        [Description("Идентификатор тарифа")]
        public Guid TarifId { get; set; }
    }
}
