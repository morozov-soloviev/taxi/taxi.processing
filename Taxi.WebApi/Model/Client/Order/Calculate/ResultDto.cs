﻿using System.ComponentModel;

namespace Taxi.WebApi.Model.Client.Order.Calculate
{
    public class ResultDto
    {
        [Description("Стоимость поездки")]
        public double Amount { get; set; }
    }
}
