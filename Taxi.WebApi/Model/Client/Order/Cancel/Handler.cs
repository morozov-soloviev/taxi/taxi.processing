﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.Domain;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Client.Order.Cancel
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result Handle(string userName, Query query)
        {
            var order = _storage.Order.Include(o => o.Client.User).FirstOrDefault(o => o.Id == query.OrderId);
            if (order == null)
            {
                return Result.Fail(Errors.IncorrectId);
            }

            if (order.Client.User.Login != userName)
            {
                return Result.Fail(Errors.UserAccessDenied);
            }

            order.Status = OrderStatus.Canceled;

            _storage.SaveChanges();

            return Result.Success();
        }
    }
}
