﻿using System;
using System.Collections.Generic;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Client.Order.Create
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        private readonly CalculateOrderService _calculateOrderService;
        private readonly AddressConverter _addressConverter;

        public Handler(TaxiContext context, UserService userService, CalculateOrderService calculateOrderService, AddressConverter addressConverter)
        {
            _storage = context;
            _userService = userService;
            _calculateOrderService = calculateOrderService;
            _addressConverter = addressConverter;
        }
        public Result<ResultDto> Handle(string userName, Query query)
        {
            var clientResult = _userService.GetClient(userName);
            if (!clientResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(clientResult.ErrorCode);
            }

            var routeResult = _addressConverter.CovertToAddress(query.Route);
            if (!routeResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(routeResult.ErrorCode);
            }

            var route = new List<RouteMember>(routeResult.Value.Count);
            for (int i = 0; i < routeResult.Value.Count; i++)
            {
                route.Add(new RouteMember
                {
                    Id = Guid.NewGuid(),
                    Address = routeResult.Value[i],
                    OrderIndex = i,
                    ArriveDate = null
                });
            }

            var tarif = _storage.Tarif.FirstOrDefault(t => t.Id == query.TarifId);
            if (tarif == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            var calculateResult = _calculateOrderService.Calculate(tarif, route.Select(m => m.Address).ToList());
            if (!calculateResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(calculateResult.ErrorCode);
            }

            var carTypes = _storage.CarType.Where(c => query.CarTypesIds.Contains(c.Id)).ToList();
            if (carTypes.Count != query.CarTypesIds.Length)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            var carOptions = _storage.CarOption.Where(c => query.CarOptionsIds.Contains(c.Id)).ToList();
            if (carOptions.Count != query.CarOptionsIds.Length)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            var order = new Domain.Order
            {
                Id = Guid.NewGuid(),
                Board = null,
                Client = clientResult.Value,
                Route = route,
                Tarif = tarif,
                CarTypes = carTypes.Select(ct => new OrderCarType { CarType = ct }).ToList(),
                CarOptions = carOptions.Select(co => new OrderCarOption { CarOption = co }).ToList(),
                CalculatedAmount = calculateResult.Value,
                Start = query.Start,
                End = null,
                Status = OrderStatus.Created
            };
            
            _storage.Order.Add(order);
            _storage.SaveChanges();

            return Result<ResultDto>.Success(new ResultDto
            {
                OrderId = order.Id
            });
        }
    }
}
