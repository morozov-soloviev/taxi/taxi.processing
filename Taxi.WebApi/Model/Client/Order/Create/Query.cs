﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Client.Order.Create
{
    public class Query
    {
        [Required]
        [Description("Маршрут")]
        public AddressDto[] Route { get; set; }

        [Required]
        [Description("Идентификатор тарифа")]
        public Guid TarifId { get; set; }

        [Required]
        [Description("Типы авто")]
        public Guid[] CarTypesIds { get; set; }

        [Required]
        [Description("Опции авто")]
        public Guid[] CarOptionsIds { get; set; }

        [Required]
        [Description("Время начала поездки")]
        public DateTime Start { get; set; }
    }
}
