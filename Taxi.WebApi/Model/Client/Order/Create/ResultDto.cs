﻿using System;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Client.Order.Create
{
    public class ResultDto
    {
        [Description("Идентификатор заказа")]
        public Guid OrderId { get; set; }
    }
}
