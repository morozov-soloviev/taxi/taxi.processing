﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Taxi.Domain;

namespace Taxi.WebApi.Model.Client.User.Register
{
    public class Query
    {
        [Required]
        [Description("Логин")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Description("Пароль")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Description("Eamil")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Description("Телфон")]
        public string Phone { get; set; }

        [Required]
        [Description("Имя")]
        public string FirstName { get; set; }

        [Required]
        [Description("Фамилия")]
        public string LastName { get; set; }

        [Description("Отчество")]
        public string MiddleName { get; set; }

        [Description("Пол")]
        public Gender? Gender { get; set; }
    }
}
