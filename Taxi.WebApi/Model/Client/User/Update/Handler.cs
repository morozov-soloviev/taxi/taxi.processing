﻿using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Client.User.Update
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }

        public Result Handle(string userName, Query query)
        {
            var clientResult = _userService.GetClient(userName);
            if (!clientResult.IsSuccess)
            {
                return Result.Fail(clientResult.ErrorCode);
            }
            var contact = clientResult.Value.User.Contact;

            if (query.Email != null)
            {
                contact.Email = query.Email;
            }
            if (query.FirstName != null)
            {
                contact.FirstName = query.FirstName;
            }
            if (query.MiddleName != null)
            {
                contact.MiddleName = query.MiddleName;
            }
            if (query.LastName != null)
            {
                contact.LastName = query.LastName;
            }
            if (query.Phone != null)
            {
                contact.Phone = query.Phone;
            }
            if (query.Gender != null)
            {
                contact.Gender = query.Gender;
            }
            if (query.Password != null)
            {
                clientResult.Value.User.Password = BCrypt.Net.BCrypt.HashPassword(query.Password);
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
