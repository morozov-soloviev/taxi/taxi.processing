﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Taxi.Domain;

namespace Taxi.WebApi.Model.Client.User.Update
{
    public class Query
    {
        [DataType(DataType.Password)]
        [Description("Пароль")]
        public string Password { get; set; }

        [DataType(DataType.EmailAddress)]
        [Description("Eamil")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Description("Телфон")]
        public string Phone { get; set; }

        [Description("Имя")]
        public string FirstName { get; set; }

        [Description("Фамилия")]
        public string LastName { get; set; }

        [Description("Отчество")]
        public string MiddleName { get; set; }

        [Description("Пол")]
        public Gender? Gender { get; set; }
    }
}
