﻿using System.Linq;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Car.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var car = _storage.Car.Where(c => c.Id == query.Id).Select(c => new CarDto
            {
                Id = c.Id,
                Owner = new UserDto
                {
                    Id = c.Owner.Id,
                    Contact = c.Owner.Contact,
                    Login = c.Owner.Login,
                    IsAdmin = c.Owner.IsAdmin,
                    IsActive = c.Owner.IsActive
                },
                Mark = c.Mark,
                Model = c.Model,
                Type = c.Type,
                Options = c.Options.Select(cco => cco.CarOption).ToList(),
                Number = c.Number,
                Year = c.Year
            }).FirstOrDefault();
            if (car == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            return Result<ResultDto>.Success(new ResultDto
            {
                Car = car
            });
        }
    }
}
