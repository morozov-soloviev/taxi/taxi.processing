﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Taxi.WebApi.Model.Data.Car.Info
{
    public class Query
    {
        [Required]
        [Description("Идентификатор")]
        public Guid Id { get; set; }
    }
}
