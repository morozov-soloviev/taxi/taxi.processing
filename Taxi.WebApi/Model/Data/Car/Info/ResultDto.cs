﻿using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Data.Car.Info
{
    public class ResultDto
    {
        [Description("Авто")]
        public CarDto Car { get; set; }
    }
}
