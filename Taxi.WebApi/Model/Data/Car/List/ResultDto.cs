﻿using System.Collections.Generic;
using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Data.Car.List
{
    public class ResultDto
    {
        [Description("Список авто")]
        public IList<CarDto> Cars { get; set; }
    }
}
