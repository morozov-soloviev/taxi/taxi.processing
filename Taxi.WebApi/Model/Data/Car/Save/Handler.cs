﻿using System;
using System.Collections.Generic;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Car.Save
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }

        public Result Handle(Query query)
        {
            Domain.Car car;
            if (query.Id != null)
            {
                car = _storage.Car.FirstOrDefault(d => d.Id == query.Id);
                if (car == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
            }
            else
            {
                car = new Domain.Car();
                _storage.Car.Add(car);
            }

            if (query.OwnerId != null)
            {
                var owner = _storage.User.FirstOrDefault(u => u.Id == query.OwnerId);
                if (owner == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
                car.Owner = owner;
            }
            if (query.CarTypeId != null)
            {
                var carType = _storage.CarType.FirstOrDefault(u => u.Id == query.CarTypeId);
                if (carType == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
                car.Type = carType;
            }
            if (query.CarOptionsIds != null)
            {
                var carOptions = new List<Domain.CarCarOption>(query.CarOptionsIds.Length);
                foreach (var carOptionId in query.CarOptionsIds)
                {
                    var carOption = _storage.CarOption.FirstOrDefault(u => u.Id == carOptionId);
                    if (carOption == null)
                    {
                        return Result.Fail(Errors.IncorrectId);
                    }
                    carOptions.Add(new CarCarOption {CarOption = carOption});
                }
                car.Options = carOptions;
            }
            if (query.Mark != null)
            {
                car.Mark = query.Mark;
            }
            if (query.Model != null)
            {
                car.Model = query.Model;
            }
            if (query.Number != null)
            {
                car.Number = query.Number;
            }
            if (query.Year != null)
            {
                car.Year = (DateTime)query.Year;
            }
            if (query.TarifRank != null)
            {
                car.TarifRank = (int)query.TarifRank;
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
