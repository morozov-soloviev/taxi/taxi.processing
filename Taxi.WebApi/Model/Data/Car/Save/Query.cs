﻿using System;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.Car.Save
{
    public class Query
    {
        [Description("Идентификатор")]
        public Guid? Id { get; set; }

        [Description("Идентификатор владельца")]
        public Guid? OwnerId { get; set; }

        [Description("Марка")]
        public string Mark { get; set; }

        [Description("Модель")]
        public string Model { get; set; }

        [Description("Тип")]
        public Guid? CarTypeId { get; set; }

        [Description("Опции")]
        public Guid[] CarOptionsIds { get; set; }

        [Description("Номер")]
        public string Number { get; set; }

        [Description("Год выпуска")]
        public DateTime? Year { get; set; }

        [Description("Ранг тарифа")]
        public int? TarifRank { get; set; }
    }
}
