﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.CarOption.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var carOption = _storage.CarOption.FirstOrDefault(c => c.Id == query.Id);
            if (carOption == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            return Result<ResultDto>.Success(new ResultDto
            {
                CarOption = carOption
            });
        }
    }
}
