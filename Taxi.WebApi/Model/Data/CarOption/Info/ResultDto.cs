﻿using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.CarOption.Info
{
    public class ResultDto
    {
        [Description("Опция авто")]
        public Domain.CarOption CarOption { get; set; }
    }
}
