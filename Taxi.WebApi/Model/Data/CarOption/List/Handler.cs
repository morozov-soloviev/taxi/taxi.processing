﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.CarOption.List
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle()
        {
            var carOptions = _storage.CarOption.ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                CarOptions = carOptions
            });
        }
    }
}
