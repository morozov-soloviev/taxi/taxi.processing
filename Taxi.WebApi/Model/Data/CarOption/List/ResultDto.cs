﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.CarOption.List
{
    public class ResultDto
    {
        [Description("Список опций авто")]
        public IList<Domain.CarOption> CarOptions { get; set; }
    }
}
