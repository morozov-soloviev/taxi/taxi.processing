﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.CarOption.Save
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }

        public Result Handle(Query query)
        {
            Domain.CarOption carOption;
            if (query.Id != null)
            {
                carOption = _storage.CarOption.FirstOrDefault(d => d.Id == query.Id);
                if (carOption == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
            }
            else
            {
                carOption = new Domain.CarOption();
                _storage.CarOption.Add(carOption);
            }

            if (query.Name != null)
            {
                carOption.Name = query.Name;
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
