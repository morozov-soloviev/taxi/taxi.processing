﻿using System;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.CarOption.Save
{
    public class Query
    {
        [Description("Идентификатор")]
        public Guid? Id { get; set; }

        [Description("Название")]
        public string Name { get; set; }
    }
}
