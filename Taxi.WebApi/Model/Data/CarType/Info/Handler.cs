﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.CarType.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var carType = _storage.CarType.FirstOrDefault(c => c.Id == query.Id);
            if (carType == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            return Result<ResultDto>.Success(new ResultDto
            {
                CarType = carType
            });
        }
    }
}
