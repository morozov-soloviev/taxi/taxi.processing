﻿using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.CarType.Info
{
    public class ResultDto
    {
        [Description("Тип авто")]
        public Domain.CarType CarType { get; set; }
    }
}
