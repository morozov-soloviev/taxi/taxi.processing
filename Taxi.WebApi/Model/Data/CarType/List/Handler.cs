﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.CarType.List
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle()
        {
            var carTypes = _storage.CarType.ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                CarTypes = carTypes
            });
        }
    }
}
