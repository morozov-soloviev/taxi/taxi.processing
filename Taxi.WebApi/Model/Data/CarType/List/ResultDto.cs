﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.CarType.List
{
    public class ResultDto
    {
        [Description("Список типов авто")]
        public IList<Domain.CarType> CarTypes { get; set; }
    }
}
