﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.CarType.Save
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }

        public Result Handle(Query query)
        {
            Domain.CarType carType;
            if (query.Id != null)
            {
                carType = _storage.CarType.FirstOrDefault(d => d.Id == query.Id);
                if (carType == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
            }
            else
            {
                carType = new Domain.CarType();
                _storage.CarType.Add(carType);
            }

            if (query.Name != null)
            {
                carType.Name = query.Name;
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
