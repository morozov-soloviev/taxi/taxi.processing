﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Driver.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var driver = _storage.Driver.Include(d => d.User).Where(c => c.Id == query.Id).Select(d => new DriverDto
            {
                Id = d.Id,
                UserId = d.User.Id,
                LicenceNumber = d.LicenceNumber,
                IsActive = d.IsActive
            }).FirstOrDefault();
            if (driver == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            return Result<ResultDto>.Success(new ResultDto
            {
                Driver = driver
            });
        }
    }
}
