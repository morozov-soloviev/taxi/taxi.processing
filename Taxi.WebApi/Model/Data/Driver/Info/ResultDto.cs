﻿using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Data.Driver.Info
{
    public class ResultDto
    {
        [Description("Водитель")]
        public DriverDto Driver { get; set; }
    }
}
