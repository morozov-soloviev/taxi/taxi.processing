﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Driver.List
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle()
        {
            var drivers = _storage.Driver.Include(d => d.User).Select(d => new DriverDto
            {
                Id = d.Id,
                UserId = d.User.Id,
                LicenceNumber = d.LicenceNumber,
                IsActive = d.IsActive
            }).ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                Drivers = drivers
            });
        }
    }
}
