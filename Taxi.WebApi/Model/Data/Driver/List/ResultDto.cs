﻿using System.Collections.Generic;
using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Data.Driver.List
{
    public class ResultDto
    {
        [Description("Список водителей")]
        public IList<DriverDto> Drivers { get; set; }
    }
}
