﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Driver.Save
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }

        public Result Handle(Query query)
        {
            Domain.Driver driver;
            if (query.Id != null)
            {
                driver = _storage.Driver.FirstOrDefault(d => d.Id == query.Id);
                if (driver == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
            }
            else
            {
                if (query.UserId == null)
                {
                    return Result.Fail("need_specify_user");
                }
                driver = new Domain.Driver();
                _storage.Driver.Add(driver);
            }

            if (query.UserId != null)
            {
                var user = _storage.User.FirstOrDefault(u => u.Id == query.UserId);
                if (user == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
                driver.User = user;
            }
            if (query.LicenceNumber != null)
            {
                driver.LicenceNumber = query.LicenceNumber;
            }
            if (query.IsActive != null)
            {
                driver.IsActive = (bool)query.IsActive;
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
