﻿using System;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.Driver.Save
{
    public class Query
    {
        [Description("Идентификатор")]
        public Guid? Id { get; set; }

        [Description("Идентификатор пользователя")]
        public Guid? UserId { get; set; }

        [Description("Номер водительского удостоверения")]
        public string LicenceNumber { get; set; }

        [Description("Подтвержден")]
        public bool? IsActive { get; set; }
    }
}
