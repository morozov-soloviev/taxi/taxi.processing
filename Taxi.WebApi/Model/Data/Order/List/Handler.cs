﻿using System.Linq;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Order.List
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle()
        {
            var orders = _storage.Order.Select(o => new OrderDto
            {
                Id = o.Id,
                Client = o.Client.User.Contact,
                Board = o.Board != null ? new BoardDto
                {
                    Id = o.Board.Id,
                    Driver = o.Board.Driver.User.Contact,
                    Car = new CarDto
                    {
                        Id = o.Board.Car.Id,
                        Mark = o.Board.Car.Mark,
                        Model = o.Board.Car.Model,
                        Type = o.Board.Car.Type,
                        Options = o.Board.Car.Options.Select(cco => cco.CarOption).ToList(),
                        Number = o.Board.Car.Number,
                        Year = o.Board.Car.Year
                    }
                } : null,
                Route = o.Route.OrderBy(r => r.OrderIndex).Select(r => new AddressDto
                {
                    Country = r.Address.Street.City.Region.Country.Name,
                    Region = r.Address.Street.City.Region.Name,
                    City = r.Address.Street.City.Name,
                    Street = r.Address.Street.Name,
                    BuildingNumber = r.Address.BuildingNumber,
                    SpecificAddress = r.Address.SpecificAddress,
                    Latitude = r.Address.Position.Latitude,
                    Longitude = r.Address.Position.Longitude
                }).ToList(),
                Tarif = new TarifDto
                {
                    Id = o.Tarif.Id,
                    Name = o.Tarif.Name,
                    Rank = o.Tarif.Rank
                },
                CarTypes = o.CarTypes.Select(oct => oct.CarType).ToList(),
                CarOptions = o.CarOptions.Select(oco => oco.CarOption).ToList(),
                CalculatedAmount = o.CalculatedAmount,
                Start = o.Start,
                End = o.End,
                Status = o.Status
            }).ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                Orders = orders
            });
        }
    }
}
