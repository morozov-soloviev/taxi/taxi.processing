﻿using System.Collections.Generic;
using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Data.Order.List
{
    public class ResultDto
    {
        [Description("Список заказов")]
        public IList<OrderDto> Orders { get; set; }
    }
}
