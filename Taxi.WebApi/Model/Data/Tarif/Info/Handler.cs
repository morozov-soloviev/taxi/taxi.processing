﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Tarif.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var tarif = _storage.Tarif.Include(d => d.Region.Country).FirstOrDefault(c => c.Id == query.Id);
            if (tarif == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            return Result<ResultDto>.Success(new ResultDto
            {
                Tarif = tarif
            });
        }
    }
}
