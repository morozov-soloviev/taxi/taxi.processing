﻿using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.Tarif.Info
{
    public class ResultDto
    {
        [Description("Тариф")]
        public Domain.Tarif Tarif { get; set; }
    }
}
