﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Tarif.List
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle()
        {
            var tarifs = _storage.Tarif.Include(d => d.Region.Country).ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                Tarifs = tarifs
            });
        }
    }
}
