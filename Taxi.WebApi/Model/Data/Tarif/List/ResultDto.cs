﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.Tarif.List
{
    public class ResultDto
    {
        [Description("Список тарифов")]
        public IList<Domain.Tarif> Tarifs { get; set; }
    }
}
