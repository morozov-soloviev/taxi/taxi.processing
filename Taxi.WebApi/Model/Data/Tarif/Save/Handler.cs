﻿using System.Linq;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.Tarif.Save
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }

        public Result Handle(Query query)
        {
            Domain.Tarif tarif;
            if (query.Id != null)
            {
                tarif = _storage.Tarif.FirstOrDefault(d => d.Id == query.Id);
                if (tarif == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
            }
            else
            {
                tarif = new Domain.Tarif();
                _storage.Tarif.Add(tarif);
            }

            if (query.Name != null)
            {
                tarif.Name = query.Name;
            }
            if (query.Rank != null)
            {
                tarif.Rank = (int)query.Rank;
            }
            if (query.DistancePrice != null)
            {
                tarif.DistancePrice = (double)query.DistancePrice;
            }
            if (query.TimePrice != null)
            {
                tarif.TimePrice = (double)query.TimePrice;
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
