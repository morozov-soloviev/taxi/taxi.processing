﻿using System;
using System.ComponentModel;

namespace Taxi.WebApi.Model.Data.Tarif.Save
{
    public class Query
    {
        [Description("Идентификатор")]
        public Guid? Id { get; set; }

        [Description("Название")]
        public string Name { get; set; }

        [Description("Ранг")]
        public int? Rank { get; set; }

        [Description("Цена за километр")]
        public double? DistancePrice { get; set; }

        [Description("Цена за минуту")]
        public double? TimePrice { get; set; }
    }
}
