﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.User.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var user = _storage.User.Include(u => u.Contact).Where(c => c.Id == query.Id).Select(u => new UserDto
            {
                Id = u.Id,
                Contact = u.Contact,
                Login = u.Login,
                IsAdmin = u.IsAdmin,
                IsActive = u.IsActive
            }).FirstOrDefault();
            if (user == null)
            {
                return Result<ResultDto>.Fail(Errors.IncorrectId);
            }

            return Result<ResultDto>.Success(new ResultDto
            {
                User = user
            });
        }
    }
}
