﻿using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Data.User.Info
{
    public class ResultDto
    {
        [Description("Пользователь")]
        public UserDto User { get; set; }
    }
}
