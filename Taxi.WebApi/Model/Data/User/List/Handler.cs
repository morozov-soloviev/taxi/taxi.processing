﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.User.List
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result<ResultDto> Handle()
        {
            var users = _storage.User.Include(u => u.Contact).Select(u => new UserDto
            {
                Id = u.Id,
                Contact = u.Contact,
                Login = u.Login,
                IsAdmin = u.IsAdmin,
                IsActive = u.IsActive
            }).ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                Users = users
            });
        }
    }
}
