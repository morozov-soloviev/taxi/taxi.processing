﻿using System.Collections.Generic;
using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Data.User.List
{
    public class ResultDto
    {
        [Description("Список пользователей")]
        public IList<UserDto> Users { get; set; }
    }
}
