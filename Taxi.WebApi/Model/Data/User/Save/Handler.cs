﻿using System;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Data.User.Save
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }

        public Result Handle(Query query)
        {
            Domain.User user;
            if (query.Id != null)
            {
                user = _storage.User.FirstOrDefault(u => u.Id == query.Id);
                if (user == null)
                {
                    return Result.Fail(Errors.IncorrectId);
                }
            }
            else
            {
                user = new Domain.User
                {
                    Id = new Guid(),
                    Contact = new Contact
                    {
                        Id = new Guid()
                    }
                };
                _storage.User.Add(user);
            }

            if (query.Email != null)
            {
                user.Contact.Email = query.Email;
            }
            if (query.FirstName != null)
            {
                user.Contact.FirstName = query.FirstName;
            }
            if (query.MiddleName != null)
            {
                user.Contact.MiddleName = query.MiddleName;
            }
            if (query.LastName != null)
            {
                user.Contact.LastName = query.LastName;
            }
            if (query.Phone != null)
            {
                user.Contact.Phone = query.Phone;
            }
            if (query.Gender != null)
            {
                user.Contact.Gender = query.Gender;
            }
            if (query.Login != null)
            {
                user.Login = query.Login;
            }
            if (query.Password != null)
            {
                user.Password = BCrypt.Net.BCrypt.HashPassword(query.Password);
            }
            if (query.IsAdmin != null)
            {
                user.IsAdmin = (bool)query.IsAdmin;
            }
            if (query.IsActive != null)
            {
                user.IsActive = (bool)query.IsActive;
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
