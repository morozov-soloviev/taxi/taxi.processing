﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Taxi.Domain;

namespace Taxi.WebApi.Model.Data.User.Save
{
    public class Query
    {
        [Description("Идентификатор")]
        public Guid? Id { get; set; }

        [Description("Логин")]
        public string Login { get; set; }

        [DataType(DataType.Password)]
        [Description("Пароль")]
        public string Password { get; set; }

        [Description("Администратор")]
        public bool? IsAdmin { get; set; }

        [Description("Подтвержден")]
        public bool? IsActive { get; set; }

        [DataType(DataType.EmailAddress)]
        [Description("Eamil")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Description("Телфон")]
        public string Phone { get; set; }

        [Description("Имя")]
        public string FirstName { get; set; }

        [Description("Фамилия")]
        public string LastName { get; set; }

        [Description("Отчество")]
        public string MiddleName { get; set; }

        [Description("Пол")]
        public Gender? Gender { get; set; }
    }
}
