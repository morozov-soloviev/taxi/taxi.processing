﻿using System;
using System.Linq;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Board.End
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }
        public Result Handle(string userName)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result.Fail(driverResult.ErrorCode);
            }

            var board = _storage.Board.FirstOrDefault(b => b.Driver.Id == driverResult.Value.Id && b.Start <= DateTime.UtcNow && (b.End == null || b.End > DateTime.UtcNow));
            if (board == null)
            {
                return Result.Fail(Errors.NoActiveBoard);
            }

            board.End = DateTime.UtcNow;

            _storage.SaveChanges();

            return Result.Success();
        }
    }
}
