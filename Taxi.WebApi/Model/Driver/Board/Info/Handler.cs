﻿using System;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Board.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }
        public Result<ResultDto> Handle(string userName)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(driverResult.ErrorCode);
            }

            var board = _storage.Board.Where(b => b.Driver.Id == driverResult.Value.Id && b.Start <= DateTime.UtcNow && (b.End == null || b.End > DateTime.UtcNow)).Select(b => new BoardDto
            {
                Id = b.Id,
                Car = new CarDto
                {
                    Id = b.Car.Id,
                    Mark = b.Car.Mark,
                    Model = b.Car.Model,
                    Type = b.Car.Type,
                    Options = b.Car.Options.Select(cco => cco.CarOption).ToList(),
                    Number = b.Car.Number,
                    Year = b.Car.Year
                }
            }).FirstOrDefault();

            var order = _storage.Order.Where(o => o.Board.Driver.Id == driverResult.Value.Id && o.Status != OrderStatus.Canceled && o.Status != OrderStatus.Completed).OrderByDescending(o => o.Start).Select(o => new OrderDto
            {
                Id = o.Id,
                Client = o.Client.User.Contact,
                Board = o.Board != null ? new BoardDto
                {
                    Id = o.Board.Id,
                    Driver = o.Board.Driver.User.Contact,
                    Car = new CarDto
                    {
                        Id = o.Board.Car.Id,
                        Mark = o.Board.Car.Mark,
                        Model = o.Board.Car.Model,
                        Type = o.Board.Car.Type,
                        Options = o.Board.Car.Options.Select(cco => cco.CarOption).ToList(),
                        Number = o.Board.Car.Number,
                        Year = o.Board.Car.Year
                    },
                    Latitude = o.Board.Position.Latitude,
                    Longitude = o.Board.Position.Longitude
                } : null,
                Route = o.Route.OrderBy(r => r.OrderIndex).Select(r => new AddressDto
                {
                    Country = r.Address.Street.City.Region.Country.Name,
                    Region = r.Address.Street.City.Region.Name,
                    City = r.Address.Street.City.Name,
                    Street = r.Address.Street.Name,
                    BuildingNumber = r.Address.BuildingNumber,
                    SpecificAddress = r.Address.SpecificAddress,
                    Latitude = r.Address.Position.Latitude,
                    Longitude = r.Address.Position.Longitude
                }).ToList(),
                Tarif = new TarifDto
                {
                    Id = o.Tarif.Id,
                    Name = o.Tarif.Name,
                    Rank = o.Tarif.Rank
                },
                CarTypes = o.CarTypes.Select(oct => oct.CarType).ToList(),
                CarOptions = o.CarOptions.Select(oco => oco.CarOption).ToList(),
                CalculatedAmount = o.CalculatedAmount,
                Start = o.Start,
                End = o.End,
                Status = o.Status
            }).FirstOrDefault();

            return Result<ResultDto>.Success(new ResultDto
            {
                IsBoardActive = board != null,
                Board = board,
                CurrentOrder = order
            });
        }
    }
}
