﻿using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Driver.Board.Info
{
    public class ResultDto
    {
        [Description("Борт активен")]
        public bool IsBoardActive { get; set; }

        [Description("Борт")]
        public BoardDto Board { get; set; }

        [Description("Текущий заказ")]
        public OrderDto CurrentOrder { get; set; }
    }
}
