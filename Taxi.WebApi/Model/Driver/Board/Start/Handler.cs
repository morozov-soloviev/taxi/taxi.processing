﻿using System;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Board.Start
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }
        public Result Handle(string userName, Query query)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result.Fail(driverResult.ErrorCode);
            }

            var board = _storage.Board.FirstOrDefault(b => b.Driver.Id == driverResult.Value.Id && b.Start <= DateTime.UtcNow && (b.End == null || b.End > DateTime.UtcNow));
            if (board != null)
            {
                return Result.Fail(Errors.BoardAlreadyActive);
            }

            var car = _storage.Car.FirstOrDefault(c => c.Id == query.CarId && c.Owner.Id == driverResult.Value.User.Id);
            if (car == null)
            {
                return Result.Fail(Errors.IncorrectId);
            }

            _storage.Board.Add(new Domain.Board
            {
                Id = Guid.NewGuid(),
                Driver = driverResult.Value,
                Car = car,
                Start = DateTime.UtcNow,
                End = null,
                Position = new Position
                {
                    Id = Guid.NewGuid(),
                    Latitude = query.Latitude,
                    Longitude = query.Longitude
                }
            });

            _storage.SaveChanges();

            return Result.Success();
        }
    }
}
