﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Taxi.WebApi.Model.Driver.Board.Start
{
    public class Query
    {
        [Required]
        [Description("Идентификатор авто")]
        public Guid CarId { get; set; }

        [Required]
        [Description("Широта")]
        public double Latitude { get; set; }

        [Required]
        [Description("Долгота")]
        public double Longitude { get; set; }
    }
}
