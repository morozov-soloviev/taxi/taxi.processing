﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Board.Update
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }
        public Result Handle(string userName, Query query)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result.Fail(driverResult.ErrorCode);
            }

            var board = _storage.Board.Include(b => b.Position).FirstOrDefault(b => b.Driver.Id == driverResult.Value.Id && b.Start <= DateTime.UtcNow && (b.End == null || b.End > DateTime.UtcNow));
            if (board == null)
            {
                return Result.Fail(Errors.NoActiveBoard);
            }

            board.Position.Latitude = query.Latitude;
            board.Position.Longitude = query.Longitude;

            _storage.SaveChanges();

            return Result.Success();
        }
    }
}
