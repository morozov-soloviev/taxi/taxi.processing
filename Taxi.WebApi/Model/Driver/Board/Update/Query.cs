﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Taxi.WebApi.Model.Driver.Board.Update
{
    public class Query
    {
        [Required]
        [Description("Широта")]
        public double Latitude { get; set; }

        [Required]
        [Description("Долгота")]
        public double Longitude { get; set; }
    }
}
