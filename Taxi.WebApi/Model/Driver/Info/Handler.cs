﻿using System.Linq;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Info
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }
        public Result<ResultDto> Handle(string userName)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(driverResult.ErrorCode);
            }

            var cars = _storage.Car.Where(c => c.Owner.Id == driverResult.Value.User.Id).Select(c => new CarDto
            {
                Id = c.Id,
                Mark = c.Mark,
                Model = c.Model,
                Type = c.Type,
                Options = c.Options.Select(cco => cco.CarOption).ToList(),
                Number = c.Number,
                Year = c.Year
            }).ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                Driver = driverResult.Value.User.Contact,
                LicenceNumber = driverResult.Value.LicenceNumber,
                Cars = cars
            });
        }
    }
}
