﻿using System.Collections.Generic;
using System.ComponentModel;
using Taxi.Domain;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Driver.Info
{
    public class ResultDto
    {
        [Description("Водитель")]
        public Contact Driver { get; set; }

        [Description("Номер водительского удостоверения")]
        public string LicenceNumber { get; set; }

        [Description("Доступные авто")]
        public IList<CarDto> Cars { get; set; }
    }
}
