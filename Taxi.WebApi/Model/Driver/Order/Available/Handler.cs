﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.Domain;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Order.Available
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }
        public Result<ResultDto> Handle(string userName)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(driverResult.ErrorCode);
            }

            //load options and types
            _storage.CarOption.ToList();
            _storage.CarType.ToList();

            var board = _storage.Board.Include(b => b.Car.Options).FirstOrDefault(b => b.Driver.Id == driverResult.Value.Id && b.Start <= DateTime.UtcNow && (b.End == null || b.End > DateTime.UtcNow));
            if (board == null)
            {
                return Result<ResultDto>.Fail(Errors.NoActiveBoard);
            }
            var carOptionsIds = board.Car.Options.Select(o => o.CarOption.Id).ToList();

            var orders = _storage.Order.Where(o => o.Status == OrderStatus.Created && o.Board == null &&
                                                   o.Tarif.Rank <= board.Car.TarifRank &&
                                                   o.CarTypes.Any(ct => board.Car.Type.Id == ct.CarType.Id) &&
                                                   o.CarOptions.All(co => carOptionsIds.Contains(co.CarOption.Id)))//link тормозит как сука. по хорошему нужно переделать
                .Select(o => new OrderDto
                {
                    Id = o.Id,
                    Client = o.Client.User.Contact,
                    Route = o.Route.OrderBy(r => r.OrderIndex).Select(r => new AddressDto
                    {
                        Country = r.Address.Street.City.Region.Country.Name,
                        Region = r.Address.Street.City.Region.Name,
                        City = r.Address.Street.City.Name,
                        Street = r.Address.Street.Name,
                        BuildingNumber = r.Address.BuildingNumber,
                        SpecificAddress = r.Address.SpecificAddress,
                        Latitude = r.Address.Position.Latitude,
                        Longitude = r.Address.Position.Longitude
                    }).ToList(),
                    Tarif = new TarifDto
                    {
                        Id = o.Tarif.Id,
                        Name = o.Tarif.Name,
                        Rank = o.Tarif.Rank
                    },
                    CarTypes = o.CarTypes.Select(oct => oct.CarType).ToList(),
                    CarOptions = o.CarOptions.Select(oco => oco.CarOption).ToList(),
                    CalculatedAmount = o.CalculatedAmount,
                    Start = o.Start,
                    End = o.End,
                    Status = o.Status
                }).ToList();

            return Result<ResultDto>.Success(new ResultDto
            {
                Orders = orders
            });
        }
    }
}
