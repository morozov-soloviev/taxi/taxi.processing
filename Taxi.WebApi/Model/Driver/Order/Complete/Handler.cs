﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.Domain;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Order.Complete
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        public Handler(TaxiContext context)
        {
            _storage = context;
        }
        public Result Handle(string userName, Query query)
        {
            var order = _storage.Order.Include(o => o.Board.Driver.User).FirstOrDefault(o => o.Id == query.OrderId);
            if (order == null)
            {
                return Result.Fail(Errors.IncorrectId);
            }

            if (order.Board.Driver.User.Login != userName)
            {
                return Result.Fail(Errors.UserAccessDenied);
            }
            if (order.Status != OrderStatus.OnWay && order.Status != OrderStatus.Waiting)
            {
                return Result.Fail(Errors.InvalidOrderStatus);
            }

            order.Status = OrderStatus.Completed;
            order.End = DateTime.UtcNow;

            _storage.SaveChanges();

            return Result.Success();
        }
    }
}
