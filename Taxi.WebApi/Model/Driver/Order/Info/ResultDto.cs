﻿using System.ComponentModel;
using Taxi.WebApi.Model.SharedDto;

namespace Taxi.WebApi.Model.Driver.Order.Info
{
    public class ResultDto
    {
        [Description("Заказ")]
        public OrderDto Order { get; set; }
    }
}
