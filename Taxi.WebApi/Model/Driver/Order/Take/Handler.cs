﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Taxi.Domain;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.Order.Take
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }
        public Result Handle(string userName, Query query)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result.Fail(driverResult.ErrorCode);
            }

            var activeOrdersCount = _storage.Order.Count(o =>
                o.Board.Driver.Id == driverResult.Value.Id && o.Status != OrderStatus.Canceled &&
                o.Status != OrderStatus.Completed);
            if (activeOrdersCount > 0)
            {
                return Result.Fail(Errors.AlreadyHaveActiveOrder);
            }

            var order = _storage.Order.Include(o => o.Board.Driver.User).Include(o => o.Tarif).Include(o => o.CarOptions).Include(o => o.CarTypes).FirstOrDefault(o => o.Id == query.OrderId);
            if (order == null)
            {
                return Result.Fail(Errors.IncorrectId);
            }

            var board = _storage.Board.Include(b => b.Car.Options).FirstOrDefault(b => b.Driver.Id == driverResult.Value.Id && b.Start <= DateTime.UtcNow && (b.End == null || b.End > DateTime.UtcNow));
            if (board == null)
            {
                return Result.Fail(Errors.NoActiveBoard);
            }

            //load options and types
            _storage.CarOption.ToList();
            _storage.CarType.ToList();

            var carOptionsIds = board.Car.Options.Select(o => o.CarOption.Id).ToList();
            if (!(order.Tarif.Rank <= board.Car.TarifRank &&
                order.CarTypes.Any(ct => board.Car.Type.Id == ct.CarType.Id) &&
                order.CarOptions.All(co => carOptionsIds.Contains(co.CarOption.Id)))) //link тормозит как сука. по хорошему нужно переделать
            {
                return Result.Fail(Errors.UserAccessDenied);
            }
            if (order.Board?.Driver.User != null)
            {
                return Result.Fail(Errors.UserAccessDenied);
            }
            if (order.Status != OrderStatus.Created)
            {
                return Result.Fail(Errors.InvalidOrderStatus);
            }

            order.Board = board;
            order.Status = OrderStatus.OnWayToClient;

            _storage.SaveChanges();

            return Result.Success();
        }
    }
}
