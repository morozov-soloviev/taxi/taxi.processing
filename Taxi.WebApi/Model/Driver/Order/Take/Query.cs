﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Taxi.WebApi.Model.Driver.Order.Take
{
    public class Query
    {
        [Required]
        [Description("Идентификатор заказа")]
        public Guid OrderId { get; set; }
    }
}
