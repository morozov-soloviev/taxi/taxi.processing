﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Taxi.Domain;

namespace Taxi.WebApi.Model.Driver.Order.Update
{
    public class Query
    {
        [Required]
        [Description("Идентификатор заказа")]
        public Guid OrderId { get; set; }

        [Required]
        [Description("Статус заказа")]
        public OrderStatus Status { get; set; }
    }
}
