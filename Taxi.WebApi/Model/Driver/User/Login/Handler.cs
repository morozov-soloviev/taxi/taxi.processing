﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Taxi.WebApi.Authorization;
using Taxi.WebApi.Services;

namespace Taxi.WebApi.Model.Driver.User.Login
{
    public class Handler
    {
        private readonly AuthOptions _authOptions;
        private readonly UserService _userService;
        public Handler(AuthOptions authOptions, UserService userService)
        {
            _authOptions = authOptions;
            _userService = userService;
        }
        public Result<ResultDto> Handle(Query query)
        {
            var driverResult = _userService.GetDriver(query.Login, query.Email, query.Phone);
            if (!driverResult.IsSuccess)
            {
                return Result<ResultDto>.Fail(Errors.WrongLoginOrPassword);
            }
            var user = driverResult.Value.User;

            if (!BCrypt.Net.BCrypt.Verify(query.Password, user.Password))
            {
                return Result<ResultDto>.Fail(Errors.WrongLoginOrPassword);
            }

            if (!user.IsActive || !driverResult.Value.IsActive)
            {
                return Result<ResultDto>.Fail(Errors.UserNeedActivation);
            }

            var identity = GetIdentity(user.Login);
            var now = DateTime.Now;
            var jwt = new JwtSecurityToken(
                    issuer: _authOptions.ISSUER,
                    audience: _authOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(_authOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(_authOptions.SymmetricSecurityKey, SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return Result<ResultDto>.Success(
                new ResultDto
                {
                    Token = encodedJwt
                });
        }

        private ClaimsIdentity GetIdentity(string login)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, AuthConstants.DriverTokenRole)
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }
    }
}
