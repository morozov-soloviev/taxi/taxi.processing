﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Taxi.WebApi.Model.Driver.User.Login
{
    public class Query
    {
        [Description("Логин")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Description("Пароль")]
        public string Password { get; set; }

        [DataType(DataType.EmailAddress)]
        [Description("Eamil")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Description("Телфон")]
        public string Phone { get; set; }
    }
}
