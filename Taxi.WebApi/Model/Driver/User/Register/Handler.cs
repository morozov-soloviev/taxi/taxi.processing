﻿using System;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.User.Register
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }

        public Result Handle(Query query)
        {
            Domain.User user;
            var userResult = _userService.GetUser(query.Login, query.Email, query.Phone);
            if (userResult.IsSuccess)
            {
                user = userResult.Value;
                var driverResult = _userService.GetDriver(user);
                if (driverResult.IsSuccess)
                {
                    return Result.Fail(Errors.UserAlreadyExist);
                }
            }
            else
            {
                user = new Domain.User
                {
                    Id = Guid.NewGuid(),
                    Login = query.Login,
                    Password = BCrypt.Net.BCrypt.HashPassword(query.Password),
                    Contact = new Domain.Contact
                    {
                        Id = Guid.NewGuid(),
                        FirstName = query.FirstName,
                        MiddleName = query.MiddleName,
                        LastName = query.LastName,
                        Phone = query.Phone,
                        Email = query.Email,
                        Gender = query.Gender
                    },
                    IsAdmin = false,
                    IsActive = true
                };
            }

            _storage.Driver.Add(new Domain.Driver
            {
                Id = Guid.NewGuid(),
                User = user,
                LicenceNumber = query.LicenceNumber,
                IsActive = false
            });

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
