﻿using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Model.Driver.User.Update
{
    public class Handler
    {
        private readonly TaxiContext _storage;
        private readonly UserService _userService;
        public Handler(TaxiContext context, UserService userService)
        {
            _storage = context;
            _userService = userService;
        }

        public Result Handle(string userName, Query query)
        {
            var driverResult = _userService.GetDriver(userName);
            if (!driverResult.IsSuccess)
            {
                return Result.Fail(driverResult.ErrorCode);
            }
            var contact = driverResult.Value.User.Contact;

            if (query.Email != null)
            {
                contact.Email = query.Email;
            }
            if (query.FirstName != null)
            {
                contact.FirstName = query.FirstName;
            }
            if (query.MiddleName != null)
            {
                contact.MiddleName = query.MiddleName;
            }
            if (query.LastName != null)
            {
                contact.LastName = query.LastName;
            }
            if (query.Phone != null)
            {
                contact.Phone = query.Phone;
            }
            if (query.Gender != null)
            {
                contact.Gender = query.Gender;
            }
            if (query.LicenceNumber != null)
            {
                driverResult.Value.LicenceNumber = query.LicenceNumber;
            }
            if (query.Password != null)
            {
                driverResult.Value.User.Password = BCrypt.Net.BCrypt.HashPassword(query.Password);
            }

            _storage.SaveChanges();
            return Result.Success();
        }
    }
}
