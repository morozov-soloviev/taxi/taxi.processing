﻿using System.Collections.Generic;
using System.Linq;

namespace Taxi.WebApi.Model
{
    public struct Result<T>
    {
        public T Value { get; private set; }
        public bool IsSuccess { get; private set; }
        public string ErrorCode { get; private set; }
        public string[] Warnings { get; private set; }

        public static Result<T> Fail(string errorCode)
        {
            return new Result<T>()
            {
                Warnings = new string[0],
                ErrorCode = errorCode,
                IsSuccess = false
            };
        }

        public static Result<T> Success(T value)
        {
            return new Result<T>()
            {
                Warnings = new string[0],
                IsSuccess = true,
                Value = value
            };
        }

        public static Result<T> Success(T value, ICollection<string> warnings)
        {
            return new Result<T>()
            {
                Warnings = warnings != null ? warnings.ToArray() : new string[0],
                IsSuccess = true,
                Value = value
            };
        }
    }

    public struct Result
    {
        public bool IsSuccess { get; private set; }
        public string ErrorCode { get; private set; }
        public string[] Warnings { get; private set; }

        public static Result Fail(string errorCode)
        {
            return new Result()
            {
                Warnings = new string[0],
                ErrorCode = errorCode,
                IsSuccess = false
            };
        }

        public static Result Success()
        {
            return new Result()
            {
                Warnings = new string[0],
                IsSuccess = true
            };
        }

        public static Result Success(ICollection<string> warnings)
        {
            return new Result()
            {
                Warnings = warnings != null ? warnings.ToArray() : new string[0],
                IsSuccess = true
            };
        }
    }
}
