﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Taxi.WebApi.Model.SharedDto
{
    public class AddressDto
    {
        [Description("Страна")]
        public string Country { get; set; }

        [Description("Регион")]
        public string Region { get; set; }

        [Description("Город")]
        public string City { get; set; }

        [Description("Улица")]
        public string Street { get; set; }

        [Description("Номер дома")]
        public string BuildingNumber { get; set; }

        [Description("Уточнение адреса")]
        public string SpecificAddress { get; set; }

        [Required]
        [Description("Широта")]
        public double Latitude { get; set; }

        [Required]
        [Description("Долгота")]
        public double Longitude { get; set; }
    }
}
