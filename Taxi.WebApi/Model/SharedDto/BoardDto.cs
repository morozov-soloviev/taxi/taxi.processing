﻿using System;
using System.ComponentModel;
using Taxi.Domain;

namespace Taxi.WebApi.Model.SharedDto
{
    public class BoardDto
    {
        [Description("Идентификатор борта")]
        public Guid Id { get; set; }

        [Description("Водитель")]
        public Contact Driver { get; set; }

        [Description("Авто")]
        public CarDto Car { get; set; }

        [Description("Широта")]
        public double? Latitude { get; set; }

        [Description("Долгота")]
        public double? Longitude { get; set; }
    }
}
