﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Taxi.Domain;

namespace Taxi.WebApi.Model.SharedDto
{
    public class CarDto
    {
        [Description("Идентификатор авто")]
        public Guid Id { get; set; }

        [Description("Владелец")]
        public UserDto Owner { get; set; }

        [Description("Марка")]
        public string Mark { get; set; }

        [Description("Модель")]
        public string Model { get; set; }

        [Description("Тип")]
        public CarType Type { get; set; }

        [Description("Опции")]
        public ICollection<CarOption> Options { get; set; }

        [Description("Номер")]
        public string Number { get; set; }

        [Description("Год выпуска")]
        public DateTime Year { get; set; }
    }
}
