﻿using System;

namespace Taxi.WebApi.Model.SharedDto
{
    public class DriverDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string LicenceNumber { get; set; }
        public bool IsActive { get; set; }
    }
}
