﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Taxi.Domain;

namespace Taxi.WebApi.Model.SharedDto
{
    public class OrderDto
    {
        [Description("Идентификатор заказа")]
        public Guid Id { get; set; }

        [Description("Клиент")]
        public Contact Client { get; set; }

        [Description("Борт")]
        public BoardDto Board { get; set; }

        [Description("Маршрут")]
        public IList<AddressDto> Route { get; set; }

        [Description("Тариф")]
        public TarifDto Tarif { get; set; }

        [Description("Типы авто")]
        public ICollection<CarType> CarTypes { get; set; }

        [Description("Опции авто")]
        public ICollection<CarOption> CarOptions { get; set; }

        [Description("Расчетная стоимость")]
        public double CalculatedAmount { get; set; }

        [Description("Время начала")]
        public DateTime Start { get; set; }

        [Description("Время завершения")]
        public DateTime? End { get; set; }

        [Description("Идентификатор заказа")]
        public OrderStatus Status { get; set; }
    }
}
