﻿using System;
using System.ComponentModel;

namespace Taxi.WebApi.Model.SharedDto
{
    public class TarifDto
    {

        [Description("Идентификатор тарифа")]
        public Guid Id { get; set; }

        [Description("Название")]
        public string Name { get; set; }

        [Description("Ранг")]
        public int Rank { get; set; }
    }
}
