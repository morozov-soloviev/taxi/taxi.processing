﻿using System;
using Taxi.Domain;

namespace Taxi.WebApi.Model.SharedDto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public Contact Contact { get; set; }
        public string Login { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsActive { get; set; }
    }
}
