﻿using System.ComponentModel;

namespace Taxi.WebApi.Model.User.Login
{
    public class ResultDto
    {
        [Description("Токен авторизации")]
        public string Token { get; set; }
    }
}
