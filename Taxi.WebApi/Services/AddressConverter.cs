﻿using System;
using System.Collections.Generic;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Model;
using Taxi.WebApi.Model.SharedDto;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Services
{
    public class AddressConverter
    {
        private readonly TaxiContext _storage;

        public AddressConverter(TaxiContext storage)
        {
            _storage = storage;
        }

        public Result<IList<Address>> CovertToAddress(IList<AddressDto> routeDto)
        {
            var route = new List<Address>(routeDto.Count);

            foreach (var addressDto in routeDto)
            {
                route.Add(CovertToAddress(addressDto));
            }

            return Result<IList<Address>>.Success(route);
        }

        private Address CovertToAddress(AddressDto addressDto)
        {
            var country = _storage.Country.FirstOrDefault(c => c.Name == addressDto.Country) ??
                          new Country { Id = Guid.NewGuid(), Name = addressDto.Country };
            var region = _storage.Region.FirstOrDefault(c => c.Country.Id == country.Id && c.Name == addressDto.Region) ??
                         new Region { Id = Guid.NewGuid(), Country = country, Name = addressDto.Region };
            var city = _storage.City.FirstOrDefault(c => c.Region.Id == region.Id && c.Name == addressDto.City) ??
                       new City { Id = Guid.NewGuid(), Region = region, Name = addressDto.City };
            var street = _storage.Street.FirstOrDefault(c => c.City.Id == city.Id && c.Name == addressDto.Street) ??
                         new Street { Id = Guid.NewGuid(), City = city, Name = addressDto.Street };

            return new Address
            {
                Id = Guid.NewGuid(),
                Street = street,
                BuildingNumber = addressDto.BuildingNumber,
                SpecificAddress = addressDto.SpecificAddress,
                Position = new Position
                {
                    Id = Guid.NewGuid(),
                    Latitude = addressDto.Latitude,
                    Longitude = addressDto.Longitude
                }
            };
        }

        public Result<IList<AddressDto>> CovertToDto(IList<Address> route)
        {
            var routeDto = new List<AddressDto>(route.Count);

            foreach (var address in route)
            {
                routeDto.Add(CovertToDto(address));
            }

            return Result<IList<AddressDto>>.Success(routeDto);
        }

        private AddressDto CovertToDto(Address address)
        {
            return new AddressDto
            {
                Country = address.Street.City.Region.Country.Name,
                Region = address.Street.City.Region.Name,
                City = address.Street.City.Name,
                Street = address.Street.Name,
                BuildingNumber = address.BuildingNumber,
                SpecificAddress = address.SpecificAddress,
                Latitude = address.Position.Latitude,
                Longitude = address.Position.Longitude
            };
        }
    }
}
