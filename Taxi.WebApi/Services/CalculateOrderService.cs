﻿using System;
using System.Collections.Generic;
using Taxi.Domain;
using Taxi.WebApi.Model;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Services
{
    public class CalculateOrderService
    {
        private readonly TaxiContext _storage;

        public CalculateOrderService(TaxiContext storage)
        {
            _storage = storage;
        }

        public Result<double> Calculate(Tarif tarif, IList<Address> route)
        {
            double distanceInMeters = 0;
            for (int i = 1; i < route.Count; i++)
            {
                distanceInMeters += GeodesicDistanceService.GetDistanceInMeters(route[i - 1].Position, route[i].Position);
            }

            var amount = distanceInMeters * tarif.DistancePrice / 1000;
            amount = Math.Ceiling(amount);
            return Result<double>.Success(amount);
        }
    }
}
