﻿using System;
using Taxi.Domain;

namespace Taxi.WebApi.Services
{
    public static class GeodesicDistanceService
    {
        public static double GetDistanceInMeters(Position a, Position b)
        {
            const double EarthRadiusInMeters = 6372797.560856d;
            const double DegreeesToRad = 0.017453292519943295769236907684886;

            double dtheta = (a.Latitude - b.Latitude) * DegreeesToRad;
            double dlambda = (a.Longitude - b.Longitude) * DegreeesToRad;
            double mean_t = (a.Latitude + b.Latitude) * DegreeesToRad / 2.0;
            double cos_meant = Math.Cos(mean_t);

            return EarthRadiusInMeters * Math.Sqrt(dtheta * dtheta + cos_meant * cos_meant * dlambda * dlambda);
        }
    }
}
