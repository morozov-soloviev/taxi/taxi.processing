﻿using System.Text;

namespace Taxi.WebApi.Services
{
    public class PhoneConverter
    {
        public static string ConvertToUniversalFormat(string text)
        {
            var phone = new StringBuilder();
            foreach (var symbol in text)
            {
                if (char.IsDigit(symbol))
                {
                    phone.Append(symbol);
                }
            }

            if (phone.Length == 11 && phone[0] == '8')
            {
                phone[0] = '7';
            }

            return phone.ToString();
        }
    }
}
