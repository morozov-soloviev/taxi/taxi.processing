﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using Taxi.Domain;
using Taxi.WebApi.Model;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi.Services
{
    public class UserService
    {
        private readonly TaxiContext _storage;

        public UserService(TaxiContext storage)
        {
            _storage = storage;
        }

        public Result<User> GetUser(string login, string phone = null, string email = null)
        {
            User user = null;
            if (!string.IsNullOrEmpty(login))
            {
                user = _storage.User.Include(u => u.Contact).FirstOrDefault(u => u.Login == login);
            }
            else if (!string.IsNullOrEmpty(phone))
            {
                var unifiedPhone = PhoneConverter.ConvertToUniversalFormat(phone);
                user = _storage.User.Include(u => u.Contact).FirstOrDefault(u => u.Contact.Phone == unifiedPhone);
            }
            else if (!string.IsNullOrEmpty(email))
            {
                user = _storage.User.Include(u => u.Contact).FirstOrDefault(u => u.Contact.Email == email);
            }
            if (user == null)
            {
                return Result<User>.Fail(Errors.UserNotFound);
            }

            return Result<User>.Success(user);
        }

        public Result<Client> GetClient(string login, string phone = null, string email = null)
        {
            Client client = null;
            if (!string.IsNullOrEmpty(login))
            {
                client = _storage.Client.Include(c => c.User.Contact).FirstOrDefault(c => c.User.Login == login);
            }
            else if (!string.IsNullOrEmpty(phone))
            {
                var unifiedPhone = PhoneConverter.ConvertToUniversalFormat(phone);
                client = _storage.Client.Include(c => c.User.Contact).FirstOrDefault(c => c.User.Contact.Phone == unifiedPhone);
            }
            else if (!string.IsNullOrEmpty(email))
            {
                client = _storage.Client.Include(c => c.User.Contact).FirstOrDefault(c => c.User.Contact.Email == email);
            }
            if (client == null)
            {
                return Result<Client>.Fail(Errors.UserNotFound);
            }

            return Result<Client>.Success(client);
        }

        public Result<Client> GetClient(User user)
        {
            Client client  = _storage.Client.Include(c => c.User.Contact).FirstOrDefault(c => c.User.Id == user.Id);
            if (client == null)
            {
                return Result<Client>.Fail(Errors.UserNotFound);
            }

            return Result<Client>.Success(client);
        }

        public Result<Driver> GetDriver(string login, string phone = null, string email = null)
        {
            Driver driver = null;
            if (!string.IsNullOrEmpty(login))
            {
                driver = _storage.Driver.Include(c => c.User.Contact).FirstOrDefault(d => d.User.Login == login);
            }
            else if (!string.IsNullOrEmpty(phone))
            {
                var unifiedPhone = PhoneConverter.ConvertToUniversalFormat(phone);
                driver = _storage.Driver.Include(c => c.User.Contact).FirstOrDefault(d => d.User.Contact.Phone == unifiedPhone);
            }
            else if (!string.IsNullOrEmpty(email))
            {
                driver = _storage.Driver.Include(c => c.User.Contact).FirstOrDefault(d => d.User.Contact.Email == email);
            }
            if (driver == null)
            {
                return Result<Driver>.Fail(Errors.UserNotFound);
            }

            return Result<Driver>.Success(driver);
        }

        public Result<Driver> GetDriver(User user)
        {
            Driver driver = _storage.Driver.Include(c => c.User.Contact).FirstOrDefault(c => c.User.Id == user.Id);
            if (driver == null)
            {
                return Result<Driver>.Fail(Errors.UserNotFound);
            }

            return Result<Driver>.Success(driver);
        }
    }
}
