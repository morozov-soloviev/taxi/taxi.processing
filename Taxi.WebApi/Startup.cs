using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Taxi.WebApi.Authorization;
using Taxi.WebApi.Services;
using Taxi.WebApi.Storage;

namespace Taxi.WebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddEntityFrameworkNpgsql();
            services.AddDbContext<TaxiContext>(options =>
                options.UseNpgsql(connection));

            // Services
            var authOptions = new AuthOptions(Configuration);
            services.AddSingleton(authOptions);
            services.AddScoped<UserService>();
            services.AddScoped<AddressConverter>();
            services.AddScoped<CalculateOrderService>();

            // User Handlers
            services.AddScoped<Model.User.Login.Handler>();

            // Client Handlers
            services.AddScoped<Model.Client.Info.Handler>();
            services.AddScoped<Model.Client.Order.Calculate.Handler>();
            services.AddScoped<Model.Client.Order.Create.Handler>();
            services.AddScoped<Model.Client.Order.Cancel.Handler>();
            services.AddScoped<Model.Client.Order.Info.Handler>();
            services.AddScoped<Model.Client.Order.List.Handler>();
            services.AddScoped<Model.Client.User.Update.Handler>();
            services.AddScoped<Model.Client.User.Register.Handler>();
            services.AddScoped<Model.Client.User.Login.Handler>();

            // Driver Handlers
            services.AddScoped<Model.Driver.Info.Handler>();
            services.AddScoped<Model.Driver.Order.Take.Handler>();
            services.AddScoped<Model.Driver.Order.Complete.Handler>();
            services.AddScoped<Model.Driver.Order.Cancel.Handler>();
            services.AddScoped<Model.Driver.Order.Update.Handler>();
            services.AddScoped<Model.Driver.Order.Info.Handler>();
            services.AddScoped<Model.Driver.Order.List.Handler>();
            services.AddScoped<Model.Driver.Order.Available.Handler>();
            services.AddScoped<Model.Driver.User.Update.Handler>();
            services.AddScoped<Model.Driver.User.Register.Handler>();
            services.AddScoped<Model.Driver.User.Login.Handler>();
            services.AddScoped<Model.Driver.Board.Info.Handler>();
            services.AddScoped<Model.Driver.Board.Start.Handler>();
            services.AddScoped<Model.Driver.Board.End.Handler>();
            services.AddScoped<Model.Driver.Board.Update.Handler>();

            // Data Handlers
            services.AddScoped<Model.Data.User.List.Handler>();
            services.AddScoped<Model.Data.User.Save.Handler>();
            services.AddScoped<Model.Data.User.Info.Handler>();
            services.AddScoped<Model.Data.Driver.List.Handler>();
            services.AddScoped<Model.Data.Driver.Save.Handler>();
            services.AddScoped<Model.Data.Driver.Info.Handler>();
            services.AddScoped<Model.Data.Tarif.List.Handler>();
            services.AddScoped<Model.Data.Tarif.Save.Handler>();
            services.AddScoped<Model.Data.Tarif.Info.Handler>();
            services.AddScoped<Model.Data.Order.List.Handler>();
            services.AddScoped<Model.Data.Order.Info.Handler>();
            services.AddScoped<Model.Data.Car.List.Handler>();
            services.AddScoped<Model.Data.Car.Save.Handler>();
            services.AddScoped<Model.Data.Car.Info.Handler>();
            services.AddScoped<Model.Data.CarOption.List.Handler>();
            services.AddScoped<Model.Data.CarOption.Save.Handler>();
            services.AddScoped<Model.Data.CarOption.Info.Handler>();
            services.AddScoped<Model.Data.CarType.List.Handler>();
            services.AddScoped<Model.Data.CarType.Save.Handler>();
            services.AddScoped<Model.Data.CarType.Info.Handler>();

            // Authorization
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = true;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidIssuer = authOptions.ISSUER,

                            ValidateAudience = true,
                            ValidAudience = authOptions.AUDIENCE,

                            ValidateLifetime = false,//

                            IssuerSigningKey = authOptions.SymmetricSecurityKey,
                            ValidateIssuerSigningKey = true,
                        };
                    });

        services.AddSwaggerDocument(settings =>
        {
            settings.Description =
                "������ API ������������ ������ ����������. ����� ���� �������� ����� ���� �������������, ��������� � ����� ���������� ������. ";
            settings.PostProcess = d => d.Info.Title = "����� API";
        });

            services
                .AddControllers(options =>
                {
                    options.OutputFormatters.RemoveType<TextOutputFormatter>();
                    options.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                    options.MaxModelValidationErrors = 4;
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                    options.JsonSerializerOptions.AllowTrailingCommas = true;
                    options.JsonSerializerOptions.ReadCommentHandling = JsonCommentHandling.Skip;
                    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                });

            services.AddHealthChecks();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseReDoc();

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseHealthChecks("/health");

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
