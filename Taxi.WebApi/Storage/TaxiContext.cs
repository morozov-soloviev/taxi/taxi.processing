﻿using System;
using Microsoft.EntityFrameworkCore;
using Taxi.Domain;

namespace Taxi.WebApi.Storage
{
    public class TaxiContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Driver> Driver { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Car> Car { get; set; }
        public DbSet<CarType> CarType { get; set; }
        public DbSet<CarOption> CarOption { get; set; }
        public DbSet<Board> Board { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Tarif> Tarif { get; set; }
        public DbSet<Rate> Rate { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Region> Region { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Street> Street { get; set; }

        public TaxiContext(DbContextOptions<TaxiContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.HasPostgresExtension("uuid-ossp");

            builder.Entity<CarCarOption>().HasKey("CarId", "CarOptionId");

            builder.Entity<OrderCarType>().HasKey("OrderId", "CarTypeId");

            builder.Entity<OrderCarOption>().HasKey("OrderId", "CarOptionId");

            builder.Entity<User>().HasData(new User
            {
                Id = new Guid("555537dd-aa9e-49ae-95c7-e4cbb2307fbd"),
                Login = "Supervisor",
                Password = "$2a$11$f3WtFCSAdStJhj9thqGZG.BZGVTSpQI7.qQFGstsdXt/tvclORcti",//Supervisor
                IsAdmin = true,
                IsActive = true
            });

            builder.Entity<Tarif>().HasData(new Tarif
            {
                Id = new Guid("16e362be-73fc-4319-bcba-8b423c05792a"),
                Name = "Эконом",
                Rank = 0,
                DistancePrice = 15,
                TimePrice = 8
            }, new Tarif
            {
                Id = new Guid("a5f9d4ec-0a14-4f16-95a4-7f89da5bed68"),
                Name = "Бизнес",
                Rank = 1,
                DistancePrice = 20,
                TimePrice = 8
            }, new Tarif
            {
                Id = new Guid("764cb89f-e8e6-4174-85be-c6e7425f6a4f"),
                Name = "Люкс",
                Rank = 2,
                DistancePrice = 25,
                TimePrice = 10
            }, new Tarif
            {
                Id = new Guid("7d929aff-0781-4b95-9590-a0c4dcb98fe5"),
                Name = "Премиум",
                Rank = 3,
                DistancePrice = 30,
                TimePrice = 12
            });

            builder.Entity<CarType>().HasData(new CarType
            {
                Id = new Guid("0281c710-6e53-4d49-a219-73a27a8d3d5f"),
                Name = "Седан"
            }, new CarType
            {
                Id = new Guid("1bafafbd-5ec7-4dbb-8d7d-a1b1b43ba866"),
                Name = "Универсал"
            }, new CarType
            {
                Id = new Guid("a245f9a1-fa31-4552-bf88-4e4689a0b861"),
                Name = "Хетчбек"
            }, new CarType
            {
                Id = new Guid("c3862077-3574-4142-98c9-0aab05c0dd0a"),
                Name = "Фургон"
            });

            builder.Entity<CarOption>().HasData(new CarOption
            {
                Id = new Guid("b98ab410-bdce-4aea-9300-48f41371873c"),
                Name = "Детское сиденье"
            }, new CarOption
            {
                Id = new Guid("c2d045b7-483d-49fc-9133-f5f8fe5058ca"),
                Name = "Домашнее животное"
            }, new CarOption
            {
                Id = new Guid("4166cfc2-46b2-44a3-9fa2-c21b4d0f12d8"),
                Name = "Доставка"
            });
        }
    }
}
